/* This is the background event page
   as specified here: https://developer.chrome.com/extensions/event_pages

   It contains global runtime stuff.
*/

var default_replacements = [
    [ 'de nicolas sarkozy' , [
	'de Paul Bismuth',
	'de Naboléon',
	'du Nain',
	'de La Caillera du 9-2',
	'de Karcherman',
	'de Big Millions',
	'de Nicolas Bruni',
	'de Monseigneur Nicolas Paul Stéphane Sarközy de Nagy-Bocsa',
	'de Nicolas 1er',
	'de Kadhafette',
	'du Bande à Nanard',
	'du Sondeur Masqué',
	'de Yelosubmarine',
	'de Berluscozy',
	'de Berluscozy',
	'de Berluscozy',
	'de Papy Zinzin',
	'de Nicro de Boucher',
	"du Rentreur Dans l'Histoire",
	'de Nicolas Swarovski',
	'de L\'homme à la Rolex d\'or',
	'de La Folie Des Glandeurs',
	'de Joe Dalton',
	'du Parrain',
	'de Sarkoléon',
	'de Notre Raymond National',
	'de L\'homme aux 6 cerveaux',
    'de Schtroumpf Grognon',
    'du Petit Père du Peuple de droite'
    ]],

    [ 'nicolas sarkozy' , [
	'Paul Bismuth',
	'Naboléon',
	'Le Nain',
	'La Caillera du 9-2',
	'Karcherman',
	'Big Millions',
	'Nicolas Bruni',
	'Monseigneur Nicolas Paul Stéphane Sarközy de Nagy-Bocsa',
	'Nicolas 1er',
	'Kadhafette',
	'Le Bande à Nanard',
	'Le Sondeur Masqué',
	'Yelosubmarine',
	'Berluscozy',
	'Berluscozy',
	'Berluscozy',
	'Papy Zinzin',
	'Nicro de Boucher',
	"Le Rentreur Dans l'Histoire",
	'Nicolas Swarovski',
	'L\'homme à la Rolex d\'or',
	'La Folie Des Glandeurs',
	'Joe Dalton',
	'Le Parrain',
	'Sarkoléon',
	'Notre Raymond National',
	'L\'homme aux 6 cerveaux',
    'Schtroumpf Grognon',
    'Le Petit Père du Peuple de droite'
    ]],
      
    [ 'sarkozy', [
	'Le Nain',
	'Berluscozy',
    'Schtroumpf Grognon',
	'Yelosubmarine',
	'Notre Raymond'
    ]]
];


var default_blacklisted_sites =
    [ "docs.google.com",
      "gmail.com",
      "mail.google.com",
      "mail.yahoo.com",
      "outlook.com"
    ];


/* An array of blacklisted sites
   according to the default and the (future)
   configuration
*/
function getBlacklist(){
    return default_blacklisted_sites;
}

function getReplacements(){
    return default_replacements;
}


function sayHello(){
    console.log('Hello');
}

function inBlacklist(url, blacklist) {
    url = url.toLowerCase() || "";
    blacklist = blacklist || [];
    for (var i = blacklist.length - 1; i >= 0; i--) {
        if (url.indexOf(blacklist[i]) > -1) {
            return true;
        }
    };
    return false;
}


/* Install the substitution on the given tab
 */

function substituteInTab( tabId, info, tab ){
    // Avoid warning on chrome tabs
    if( tab.url.indexOf('chrome') == 0 ){
	return;
    }

    if( inBlacklist(tab.url , getBlacklist() ) ){
	return;
    }

    // Active or not?
    chrome.storage.sync.get("status", function(result){
	if( ( result['status'] || "enabled" ) === 'enabled' ){
	    chrome.tabs.executeScript( tabId,{
		file: "js/substitution.js",
		runAt: "document_end",
	    });
	}
    });
}


function handleMessage(request, sender , sendResponse){
    if( request.get === "replacements" ){
	sendResponse( getReplacements() );
	return true;
    }
    console.error("Don't know how to handle request",  request );

    return false;
}

function toggleActive() {
    chrome.storage.sync.get("status", function(result) {
	var status, icon, message;


	// Grab the current state.
        if (result["status"] === null) {
            status = "enabled";
        } else {
            status = result["status"];
        }

	// Calculate the next icon and message.
        if (status === "enabled") {
            icon = {"path": "images/NOSARKO-disabled.png"};
            message = {"title": "Cliquez pour activer l'extension No Sarko"};
            status = "disabled";
        } else {
            icon = {"path": "images/NOSARKO.png"};
            message = {"title": "Cliquez pour désactiver l'extension No Sarko"};
            status = "enabled";
        }
        chrome.browserAction.setIcon(icon);
        chrome.browserAction.setTitle(message);
        chrome.storage.sync.set({
            "status": status
        });
    });
}


/* Install the substitutions on any updated tabs */
chrome.tabs.onUpdated.addListener(substituteInTab);

/* Reply to messages from other parts of the plugin */
chrome.runtime.onMessage.addListener(handleMessage);

/* The toggle on the browser action button */
chrome.browserAction.onClicked.addListener(toggleActive);
