/* This gets executed on any loaded document
   by the eventPage.js installed listener
*/


chrome.runtime.sendMessage({ get: "replacements" }, function(replacements){

    var substitute = (function(){
	"use strict";
	var i;
	var replacementObjects = [];
	var replaceRegex;

	for( i = 0 ; i < replacements.length ; i++ ){
	    replaceRegex = new RegExp("\\b" + replacements[i][0]+ "\\b" , "gi");
	    replacementObjects.push({ regex: replaceRegex,
				     replaceWith: replacements[i][1]
				   });
	}

	// Activate replacements in a node
	return function(node){
	    var i;
	    var ignoreTags = { "STYLE": 0,
			       "SCRIPT": 0,
			       "NOSCRIPT": 0,
			       "IFRAME" : 0,
			       "OBJECT": 0,
			       "INPUT": 0,
			       "FORM": 0,
			       "TEXTAREA": 0
			     };
	    // Ignore the right tags
	    if( node.tagName in ignoreTags ){
		return;
	    }

	    // And do some replacements!
	    for(i = 0 ; i < replacementObjects.length; i++ ){
		// In place change!
		node.nodeValue = node.nodeValue.replace(replacementObjects[i].regex,
							function(match){
							    var replaceWith = replacementObjects[i].replaceWith;
							    // Pick a replacement randomly
							    var nickname = replaceWith[Math.floor(Math.random() * replaceWith.length)];
							    return nickname;
							});


	    }
	}

    })();



    var node, iter;
    var iter = document.createNodeIterator(document.body, NodeFilter.SHOW_TEXT);
    while ((node = iter.nextNode())) {
        substitute(node);
    }

});
